-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2021 at 02:20 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikp`
--

-- --------------------------------------------------------

--
-- Table structure for table `batas_kp_new`
--

CREATE TABLE `batas_kp_new` (
  `id_batas_kp` int(11) NOT NULL,
  `id_kp` char(11) NOT NULL,
  `tanggal_batas` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `batas_kp_new`
--

INSERT INTO `batas_kp_new` (`id_batas_kp`, `id_kp`, `tanggal_batas`) VALUES
(5, '1', '2021-12-11');

-- --------------------------------------------------------

--
-- Table structure for table `koor_kp`
--

CREATE TABLE `koor_kp` (
  `id_koor` int(20) NOT NULL,
  `nik` int(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `koor_kp`
--

INSERT INTO `koor_kp` (`id_koor`, `nik`, `nama`, `email`, `password`, `status`) VALUES
(1, 111111, 'Dosen A', 'dosena@gmail.com', 'password', 1),
(2, 222222, 'Koordinator A', 'koora@gmail.com', '123', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kp`
--

CREATE TABLE `kp` (
  `id_kp` int(11) NOT NULL,
  `tahun` int(5) NOT NULL,
  `semester` char(20) NOT NULL,
  `nim` int(50) NOT NULL,
  `penguji` char(20) NOT NULL,
  `tanggal_ujian` date NOT NULL,
  `status` int(1) NOT NULL,
  `judul` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp`
--

INSERT INTO `kp` (`id_kp`, `tahun`, `semester`, `nim`, `penguji`, `tanggal_ujian`, `status`, `judul`) VALUES
(11, 2021, 'genap', 72180212, 'argo', '2020-03-03', 0, 'aku ingin kp');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` int(8) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `google_Id` varchar(150) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prakp`
--

CREATE TABLE `prakp` (
  `id_prakp` int(11) NOT NULL,
  `nim` int(50) NOT NULL,
  `tahun` int(5) NOT NULL,
  `semester` char(10) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `pembimbing` varchar(20) NOT NULL,
  `status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prakp`
--

INSERT INTO `prakp` (`id_prakp`, `nim`, `tahun`, `semester`, `judul`, `pembimbing`, `status`) VALUES
(14, 72180212, 20201, 'genap', 'aku kp', 'katon', 'Terima');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keterangan`
--

CREATE TABLE `surat_keterangan` (
  `id_skp` int(11) NOT NULL,
  `tahun` int(5) NOT NULL,
  `semester` char(10) NOT NULL,
  `nim` int(50) NOT NULL,
  `isi_surat` char(100) DEFAULT NULL,
  `status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat_keterangan`
--

INSERT INTO `surat_keterangan` (`id_skp`, `tahun`, `semester`, `nim`, `isi_surat`, `status`) VALUES
(4, 2021, 'genap', 72180212, 'belum bisa uploud filed', 'Terima');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kp`
-- (See below for the actual view)
--
CREATE TABLE `view_kp` (
`id_kp` int(11)
,`tahun` int(5)
,`semester` char(20)
,`nim` int(50)
,`penguji` char(20)
,`tanggal_ujian` date
,`status` int(1)
,`judul` text
,`nik` int(20)
,`nama` varchar(50)
,`email` varchar(50)
,`password` varchar(50)
,`status_koor` int(1)
,`tanggal_batas` char(50)
,`id_batas_kp` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_prakp`
-- (See below for the actual view)
--
CREATE TABLE `view_prakp` (
`id_prakp` int(11)
,`nim` int(50)
,`tahun` int(5)
,`semester` char(10)
,`judul` varchar(150)
,`pembimbing` varchar(20)
,`nik` int(20)
,`nama` varchar(50)
,`email` varchar(50)
,`password` varchar(50)
,`nama_mahasiswa` varchar(50)
,`email_mahasiswa` varchar(50)
,`password_mahasiswa` varchar(50)
,`google_Id` varchar(150)
,`updated_at` timestamp
,`created_at` timestamp
,`id` int(2)
,`status` char(10)
);

-- --------------------------------------------------------

--
-- Structure for view `view_kp`
--
DROP TABLE IF EXISTS `view_kp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kp`  AS SELECT `kp`.`id_kp` AS `id_kp`, `kp`.`tahun` AS `tahun`, `kp`.`semester` AS `semester`, `kp`.`nim` AS `nim`, `kp`.`penguji` AS `penguji`, `kp`.`tanggal_ujian` AS `tanggal_ujian`, `kp`.`status` AS `status`, `kp`.`judul` AS `judul`, `koor_kp`.`nik` AS `nik`, `koor_kp`.`nama` AS `nama`, `koor_kp`.`email` AS `email`, `koor_kp`.`password` AS `password`, `koor_kp`.`status` AS `status_koor`, `batas_kp_new`.`tanggal_batas` AS `tanggal_batas`, `batas_kp_new`.`id_batas_kp` AS `id_batas_kp` FROM ((`kp` left join `koor_kp` on(`kp`.`penguji` = `koor_kp`.`id_koor`)) left join `batas_kp_new` on(`batas_kp_new`.`id_kp` = `kp`.`id_kp`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_prakp`
--
DROP TABLE IF EXISTS `view_prakp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_prakp`  AS SELECT `prakp`.`id_prakp` AS `id_prakp`, `prakp`.`nim` AS `nim`, `prakp`.`tahun` AS `tahun`, `prakp`.`semester` AS `semester`, `prakp`.`judul` AS `judul`, `prakp`.`pembimbing` AS `pembimbing`, `koor_kp`.`nik` AS `nik`, `koor_kp`.`nama` AS `nama`, `koor_kp`.`email` AS `email`, `koor_kp`.`password` AS `password`, `mahasiswa`.`nama` AS `nama_mahasiswa`, `mahasiswa`.`email` AS `email_mahasiswa`, `mahasiswa`.`password` AS `password_mahasiswa`, `mahasiswa`.`google_Id` AS `google_Id`, `mahasiswa`.`updated_at` AS `updated_at`, `mahasiswa`.`created_at` AS `created_at`, `mahasiswa`.`id` AS `id`, `prakp`.`status` AS `status` FROM ((`prakp` join `koor_kp` on(`prakp`.`pembimbing` = `koor_kp`.`id_koor`)) join `mahasiswa` on(`prakp`.`nim` = `mahasiswa`.`nim`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batas_kp_new`
--
ALTER TABLE `batas_kp_new`
  ADD PRIMARY KEY (`id_batas_kp`);

--
-- Indexes for table `koor_kp`
--
ALTER TABLE `koor_kp`
  ADD PRIMARY KEY (`id_koor`);

--
-- Indexes for table `kp`
--
ALTER TABLE `kp`
  ADD PRIMARY KEY (`id_kp`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prakp`
--
ALTER TABLE `prakp`
  ADD PRIMARY KEY (`id_prakp`);

--
-- Indexes for table `surat_keterangan`
--
ALTER TABLE `surat_keterangan`
  ADD PRIMARY KEY (`id_skp`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batas_kp_new`
--
ALTER TABLE `batas_kp_new`
  MODIFY `id_batas_kp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `koor_kp`
--
ALTER TABLE `koor_kp`
  MODIFY `id_koor` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kp`
--
ALTER TABLE `kp`
  MODIFY `id_kp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prakp`
--
ALTER TABLE `prakp`
  MODIFY `id_prakp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `surat_keterangan`
--
ALTER TABLE `surat_keterangan`
  MODIFY `id_skp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
