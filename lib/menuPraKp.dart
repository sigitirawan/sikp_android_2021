
import 'package:flutter/material.dart';
import 'package:kpukdw2021/addPraKpMahasiswa.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';

class MenuPraKp extends StatefulWidget {
  const MenuPraKp({Key key}) : super(key: key);

  @override
  _MenuPraKpState createState() => _MenuPraKpState();
}

class _MenuPraKpState extends State<MenuPraKp> {

  List<PraKp> listprakp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PRAKP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>PraKpTambah()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getPraKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<PraKp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listprakp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listprakp[position].nim + " - " +
                            listprakp[position].semester + " , " + listprakp[position].tahun),
                        subtitle: Text(
                            "Judul : " + listprakp[position].judul + "\n" +
                                "Semester : " + listprakp[position].semester + "\n" +
                                "nim : " + listprakp[position].nim + "\n" +
                                "tahun : " + listprakp[position].tahun + "\n"
                                "pembimbing : " + listprakp[position].pembimbing + "\n"
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listprakp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
