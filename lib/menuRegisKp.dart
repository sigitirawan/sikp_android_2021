
import 'package:flutter/material.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';
import 'package:kpukdw2021/verifKp.dart';

class MenuRegisKp extends StatefulWidget {
  const MenuRegisKp({Key key}) : super(key: key);

  @override
  _MenuRegisKpState createState() => _MenuRegisKpState();
}

class _MenuRegisKpState extends State<MenuRegisKp> {
  List<KP> listKp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<KP>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listKp[position].nim + " - " +
                              listKp[position].semester + " , " + listKp[position].tahun),
                          subtitle: Text(
                              "Judul : " + listKp[position].judul + "\n" +
                                  "Status : " + listKp[position].status
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>VerifKp()));
                    },
                  );
                },
                itemCount: listKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
