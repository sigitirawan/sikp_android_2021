
import 'package:flutter/material.dart';
import 'package:kpukdw2021/addKpMahasiswa.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';

class Menukp extends StatefulWidget {
  const Menukp({Key key}) : super(key: key);

  @override
  _MenukpState createState() => _MenukpState();
}

class _MenukpState extends State<Menukp> {

  List<KP> listkp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Kp()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<KP>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listkp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listkp[position].nim + " - " +
                            listkp[position].semester + " , " + listkp[position].tahun),
                        subtitle: Text(
                            "Judul : " + listkp[position].judul + "\n" +
                                "Semester : " + listkp[position].semester + "\n" +
                                "nim : " + listkp[position].nim + "\n" +
                                "tahun : " + listkp[position].tahun + "\n"
                                "penguji : " + listkp[position].penguji + "\n"
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listkp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
