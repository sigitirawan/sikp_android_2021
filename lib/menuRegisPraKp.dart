
import 'package:flutter/material.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';
import 'package:kpukdw2021/praKpVerif.dart';

class MenuRegisPraKp extends StatefulWidget {
  const MenuRegisPraKp({Key key}) : super(key: key);

  @override
  _MenuRegisPraKpState createState() => _MenuRegisPraKpState();
}

class _MenuRegisPraKpState extends State<MenuRegisPraKp> {
  List<PraKp> listprakp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PraKP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getPraKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<PraKp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listprakp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listprakp[position].nim + " - " +
                              listprakp[position].semester + " , " + listprakp[position].tahun),
                          subtitle: Text(
                              "Judul : " + listprakp[position].judul + "\n" +
                                  "Status : " + listprakp[position].status
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>PraKpVerif()));
                    },
                  );
                },
                itemCount: listprakp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
