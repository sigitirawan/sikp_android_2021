

import 'package:flutter/material.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';

class MenuBimbinganDosen extends StatefulWidget {
  const MenuBimbinganDosen({Key key}) : super(key: key);

  @override
  _MenuBimbinganDosenState createState() => _MenuBimbinganDosenState();
}

class _MenuBimbinganDosenState extends State<MenuBimbinganDosen> {
  List<PraKp> listprakp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DaftarBimbinganDosen"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getbimbingandosen(),
          builder: (BuildContext context,
              AsyncSnapshot<List<PraKp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listprakp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listprakp[position].nim + " - " +
                              listprakp[position].semester + " , " + listprakp[position].tahun),
                          subtitle: Text(
                              "Judul : " + listprakp[position].judul + "\n" +
                                  "Status : " + listprakp[position].status
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                    },
                  );
                },
                itemCount: listprakp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}


