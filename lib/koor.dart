
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kpukdw2021/addBatasKp.dart';
import 'package:kpukdw2021/menuBatasKp.dart';
import 'package:kpukdw2021/menuDosenBimbinganKoor.dart';
import 'package:kpukdw2021/menuRegisKp.dart';
import 'package:kpukdw2021/menuRegisPraKp.dart';
import 'package:kpukdw2021/menuSurat.dart';
import 'package:kpukdw2021/model.dart';

import 'main.dart';

class Koor extends StatefulWidget {
  final String nama, email, foto;

  Koor({this.nama, this.email, this.foto});
  @override
  _KoorState createState() => _KoorState();
}

class _KoorState extends State<Koor> {
  Future<void> _keluar() async{
    await GoogleSignIn().signOut();

    Navigator.pushReplacement(context,
        MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("MENU UTAMA KOOR"),
        backgroundColor: Colors.green[800],
      ),
      backgroundColor: Colors.green[100],

      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("${widget.nama}", style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 17.0),),
              accountEmail: new Text(" ${widget.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(widget.foto),
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
              onTap: () => _keluar(),
            ),
          ],
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Bk()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.settings_applications_sharp, size: 60.0,
                        color: Colors.red,),
                      Text("Set Batas Pelaksanaan KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.settings_applications_sharp, size: 60.0,
                        color: Colors.red,),
                      Text("Pengaturan Ujian KP", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuRegisPraKp()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 60.0,
                        color: Colors.blue,),
                      Text("Daftar Regis PRAKP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuRegisKp()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 60.0, color: Colors.blue,),
                      Text(
                          "Daftar Regis KP", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuSurat()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 60.0, color: Colors.blue,),
                      Text("Pengajuan SK",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuDosenBimbingan()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 60.0, color: Colors.blue,),
                      Text("Daftar Bimbingan KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 60.0, color: Colors.blue,),
                      Text("Daftar Ujian KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
