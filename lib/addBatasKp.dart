import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class Bataskp extends StatefulWidget {
  const Bataskp({Key key}) : super(key: key);

  @override
  _BataskpState createState() => _BataskpState();
}

class _BataskpState extends State<Bataskp> {
  TextEditingController tanggal_batasController = new TextEditingController();


  save() async {
    final response = await http.post(
        Uri.parse("http://192.168.18.36/db_sikp/addBataskp.php"),
        body: {
          "tanggal_batas": tanggal_batasController.text,

        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Batas KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: tanggal_batasController,
                    decoration: InputDecoration(
                        labelText: "bataskp :",
                        hintText: "contoh: set batas kp",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.datetime,
                  ),
                  SizedBox(height: 15,),

                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Batas KP"),
                              content: Text("Anda Yakin ?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: () {
                                      save();
                                      Navigator.pop(context);
                                    },
                                    child: Text("Ya")
                                ),
                                TextButton(onPressed: () {
                                  Navigator.pop(context);
                                }, child: Text("Tidak"))
                              ],
                            );
                          }
                      );
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),

                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

