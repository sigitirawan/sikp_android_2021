
import 'package:flutter/material.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';
import 'package:kpukdw2021/verifSurat.dart';

class MenuSurat extends StatefulWidget {
  const MenuSurat({Key key}) : super(key: key);

  @override
  _MenuSuratState createState() => _MenuSuratState();
}

class _MenuSuratState extends State<MenuSurat> {
  List<Sk> listsk;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SK"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getSk(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Sk>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listsk = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listsk[position].nim + " - " +
                              listsk[position].semester + " , " + listsk[position].tahun),
                          subtitle: Text(
                              "isi_surat : " + listsk[position].isi_surat + "\n" +
                                  "Status : " + listsk[position].status
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>VerifSurat()));
                    },
                  );
                },
                itemCount: listsk.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
