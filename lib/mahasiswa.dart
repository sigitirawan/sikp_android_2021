
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kpukdw2021/addKpMahasiswa.dart';
import 'package:kpukdw2021/addPraKpMahasiswa.dart';
import 'package:kpukdw2021/addSkMahasiswa.dart';
import 'package:kpukdw2021/menuPraKp.dart';
import 'package:kpukdw2021/menuSk.dart';
import 'package:kpukdw2021/menukp.dart';

import 'main.dart';

class Mahasiswa extends StatefulWidget {
  final String nama, email, foto;

  Mahasiswa({this.nama, this.email, this.foto});
  @override
  _MahasiwaState createState() => _MahasiwaState();
}

class _MahasiwaState extends State<Mahasiswa> {
  Future<void> _keluar() async{
    await GoogleSignIn().signOut();

    Navigator.pushReplacement(context,
        MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("MENU UTAMA MAHASISWA"),
        backgroundColor: Colors.green[800],
      ),
      backgroundColor: Colors.green[100],

      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("${widget.nama}", style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 17.0),),
              accountEmail: new Text(" ${widget.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(widget.foto),
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
              onTap: () => _keluar(),
            ),
          ],
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuSk()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.forward_to_inbox, size: 70.0,
                        color: Colors.green,),
                      Text("Pengajuan Surat KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Menukp()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file, size: 70.0,
                        color: Colors.deepOrangeAccent,),
                      Text("Pengajuan KP", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuPraKp()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file_outlined, size: 70.0,
                        color: Colors.deepOrange,),
                      Text("Pengajuan Pra KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
