

import 'package:flutter/material.dart';
import 'package:kpukdw2021/addBatasKp.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';

class Bk extends StatefulWidget {
  const Bk({Key key}) : super(key: key);

  @override
  _BkState createState() => _BkState();
}

class _BkState extends State<Bk> {

  List<Kpbatas> listbk;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Batas KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Bataskp()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getBatasKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Kpbatas>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listbk = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listbk[position].tanggal_batas + " - " +
                            listbk[position].id_kp + " , " + listbk[position].id_batas_kp),
                        subtitle: Text(
                            "tanggl_batas : " + listbk[position].tanggal_batas + "\n" +
                                "id_kp : " + listbk[position].id_kp
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listbk.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
