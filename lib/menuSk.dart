

import 'package:flutter/material.dart';
import 'package:kpukdw2021/addSkMahasiswa.dart';
import 'package:kpukdw2021/apiServices.dart';
import 'package:kpukdw2021/model.dart';

class MenuSk extends StatefulWidget {
  const MenuSk({Key key}) : super(key: key);

  @override
  _MenuSkState createState() => _MenuSkState();
}

class _MenuSkState extends State<MenuSk> {

  List<Sk> listsk;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SK"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>SKeterangan()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getSk(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Sk>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listsk = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listsk[position].nim + " - " +
                            listsk[position].semester + " , " + listsk[position].tahun),
                        subtitle: Text(
                            "nim : " + listsk[position].nim + "\n" +
                                "Semester : " + listsk[position].semester + "\n" +
                                "tahun : " + listsk[position].tahun + "\n"
                                "isi_surat : " + listsk[position].isi_surat + "\n"
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listsk.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}

