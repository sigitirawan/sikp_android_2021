
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PraKpVerif extends StatefulWidget {
  const PraKpVerif({Key key}) : super(key: key);

  @override
  _PraKpVerifState createState() => _PraKpVerifState();
}

class _PraKpVerifState extends State<PraKpVerif> {

  TextEditingController nimController = new TextEditingController(text: "72180212");

  saveTerima() async{
    final response = await http.post(Uri.parse("https://192.168.18.36/db_sikp/verifPraKpTerima.php"),
        body: {
          "nim": nimController.text
        });
  }

  saveTolak() async{
    final response = await http.post(Uri.parse("https://192.168.18.36/db_sikp/verifPraKpTolak.php"),
        body: {
          "nim": nimController.text
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan Pra KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  Text("Apakah anda yakin dan setuju untuk menerima Pra KP dengan nim dibawah ini ? "),
                  SizedBox(height: 15,),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                    enabled: false,
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: <Widget>[
                      ElevatedButton(
                        onPressed: (){
                          saveTerima();
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Text("Terima Yes"),
                      ),
                      Divider(
                        indent: 10,
                        endIndent: 10,
                      ),
                      ElevatedButton(
                        onPressed: (){
                          saveTolak();
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Text("Tolak No"),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
