
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class Kp extends StatefulWidget {
  const Kp({Key key}) : super(key: key);

  @override
  _KpState createState() => _KpState();
}

class _KpState extends State<Kp> {
  TextEditingController tahunController = new TextEditingController();
  TextEditingController semesterController = new TextEditingController();
  TextEditingController nimController = new TextEditingController();
  TextEditingController pengujiController = new TextEditingController();
  TextEditingController tanggal_ujianController = new TextEditingController();
  TextEditingController judulController = new TextEditingController();

  save() async {
    final response = await http.post(
        Uri.parse("http://192.168.18.36/db_sikp/addKp.php"),
        body: {
          "tahun": tahunController.text,
          "semester": semesterController.text,
          "nim": nimController.text,
          "penguji": pengujiController.text,
          "tanggal_ujian": tanggal_ujianController.text,
          "judul": judulController.text,
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        hintText: "contoh: 72180212",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: semesterController,
                    decoration: InputDecoration(
                        labelText: "Semester :",
                        hintText: "Genap / Ganjil",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: tahunController,
                    decoration: InputDecoration(
                        labelText: "Tahun : ",
                        hintText: "contoh: 2021",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: pengujiController,
                    decoration: InputDecoration(
                        labelText: "penguji :",
                        hintText: "contoh : nama penguji",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: tanggal_ujianController,
                    decoration: InputDecoration(
                        labelText: "tanggal_ujian :",
                        hintText: "contoh : hari bulan tahun",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: judulController,
                    decoration: InputDecoration(
                        labelText: "judul :",
                        hintText: "contoh : nama judul kp",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Pengajuan KP"),
                              content: Text("Anda Yakin ?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: () {
                                      save();
                                      Navigator.pop(context);
                                    },
                                    child: Text("Ya")
                                ),
                                TextButton(onPressed: () {
                                  Navigator.pop(context);
                                }, child: Text("Tidak"))
                              ],
                            );
                          }
                      );
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),

                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
