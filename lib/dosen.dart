
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kpukdw2021/menuBimbinganDosen.dart';

import 'main.dart';

class Dosen extends StatefulWidget {
  final String nama, email, foto;

  Dosen({this.nama, this.email, this.foto});
  @override
  _DosenState createState() => _DosenState();
}

class _DosenState extends State<Dosen> {
  Future<void> _keluar() async{
    await GoogleSignIn().signOut();

    Navigator.pushReplacement(context,
        MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("MENU UTAMA DOSEN"),
        backgroundColor: Colors.green[800],
      ),
      backgroundColor: Colors.green[100],

      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("${widget.nama}", style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 17.0),),
              accountEmail: new Text(" ${widget.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(widget.foto),
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
              onTap: () => _keluar(),
            ),
          ],
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuBimbinganDosen()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 70.0,
                        color: Colors.green,),
                      Text("Daftar Bimbingan Dosen",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books_sharp, size: 70.0,
                        color: Colors.deepOrangeAccent,),
                      Text("Daftar Ujian Dosen", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
