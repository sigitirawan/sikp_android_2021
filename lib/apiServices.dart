import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart' as http;
import 'package:kpukdw2021/addBatasKp.dart';
import 'package:kpukdw2021/model.dart';

class ApiServices{
  final String baseUrl = "https://192.168.18.36/db_sikp";
  Client client = Client();

  Future<List<KP>> getKp() async{
    final response = await client.get(Uri.parse("$baseUrl/getKp.php"));
    if(response.statusCode == 200){
      return kpFromJson(response.body);
    }else{
      return null;
    }
  }
  //---------------------------------------------------------------------------------------------------------------------------------
  Future<List<PraKp>> getPraKp() async{
    final response = await client.get(Uri.parse("$baseUrl/getPraKp.php"));
    if(response.statusCode == 200){
      return prakpFromJson(response.body);
    }else{
      return null;
    }
  }

  //--------------------------------------------------------------------------------------------------------------------------
  Future<List<Sk>> getSk() async{
    final response = await client.get(Uri.parse("$baseUrl/getSk.php"));
    if(response.statusCode == 200){
      return skFromJson(response.body);
    }else{
      return null;
    }
  }
  //--------------------------------------------------------------------------------------------------------------------------
  Future<List<PraKp>> getbimbingankoor() async{
    final response = await client.get(Uri.parse("$baseUrl/bimbinganKoor.php"));
    if(response.statusCode == 200){
      return prakpFromJson(response.body);
    }else{
      return null;
    }
  }
  //-------------------------------------------------------------------------------------------------------------------------
  Future<List<PraKp>> getbimbingandosen() async{
    final response = await client.get(Uri.parse("$baseUrl/bimbinganDosen.php"));
    if(response.statusCode == 200){
      return prakpFromJson(response.body);
    }else{
      return null;
    }
  }
  //-------------------------------------------------------------------------------------------------------------------------

  Future<List<Kpbatas>> getBatasKp() async{
    final response = await client.get(Uri.parse("$baseUrl/BatasKp.php"));
    if(response.statusCode == 200){
      return bataskpFromJson(response.body);
    }else{
      return null;
    }
  }
}

