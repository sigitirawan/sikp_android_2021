import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class PraKpTambah extends StatefulWidget {
  const PraKpTambah({Key key}) : super(key: key);

  @override
  _PraKpTambahState createState() => _PraKpTambahState();
}

class _PraKpTambahState extends State<PraKpTambah> {
  TextEditingController nimController = new TextEditingController();
  TextEditingController tahunController = new TextEditingController();
  TextEditingController semesterController = new TextEditingController();
  TextEditingController judulController = new TextEditingController();
  TextEditingController pembimbingController = new TextEditingController();

  save() async {
    final response = await http.post(
        Uri.parse("http://192.168.18.36/db_sikp/addPraKp.php"),
        body: {
          "nim": nimController.text,
          "tahun": tahunController.text,
          "semester": semesterController.text,
          "judul": judulController.text,
          "pembimbing": pembimbingController.text,
        });
  }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan PraKP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        hintText: "contoh: 72180212",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: tahunController,
                    decoration: InputDecoration(
                        labelText: "Tahun : ",
                        hintText: "contoh: 2021",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: semesterController,
                    decoration: InputDecoration(
                        labelText: "Semester :",
                        hintText: "Genap / Ganjil",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: judulController,
                    decoration: InputDecoration(
                        labelText: "judul :",
                        hintText: "contoh : judul PraKp",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: pembimbingController,
                    decoration: InputDecoration(
                        labelText: "pembimbing :",
                        hintText: "contoh : pembimbing",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Pengajuan Pra KP"),
                              content: Text("Anda Yakin ?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: () {
                                      save();
                                      Navigator.pop(context);
                                    },
                                    child: Text("Ya")
                                ),
                                TextButton(onPressed: () {
                                  Navigator.pop(context);
                                }, child: Text("Tidak"))
                              ],
                            );
                          }
                      );
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),

                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}