import 'dart:convert';

import 'package:kpukdw2021/addBatasKp.dart';

class KP {
  String id_kp;
  String tahun;
  String semester;
  String nim;
  String penguji;
  String tanggal_ujian;
  String status;
  String judul;


  KP(
      {this.id_kp, this.tahun, this.semester, this.nim, this.penguji, this.tanggal_ujian, this.status, this.judul});

  factory KP.fromJson(Map<String, dynamic> map){
    return KP(
        id_kp: map["id_kp"],
        tahun: map["tahun"],
        semester: map["semester"],
        nim: map["nim"],
        penguji: map["penguji"],
        tanggal_ujian: map["tanggal_ujian"],
        status: map["status"],
        judul: map["judul"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id_kp": id_kp,
      "tahun": tahun,
      "semester": semester,
      "nim": nim,
      "penguji": penguji,
      "tanggal_ujian": tanggal_ujian,
      "status": status,
      "judul": judul,
    };
  }

  @override
  String toString() {
    return 'KP{id_kp: $id_kp, tahun: $tahun, semester: $semester, nim: $nim, penguji: $penguji, tanggal_ujian: $tanggal_ujian, status: $status, judul: $judul}';
  }
}

  List<KP> kpFromJson(String jsonData) {
    final data = json.decode(jsonData);
    return List<KP>.from(data.map((item) => KP.fromJson(item)));
  }

  String kpToJson(KP data) {
    final jsonData = data.toJson();
    return json.encode(jsonData);
  }

  //-------------------------------------------------------------------------------------------------------------------

class PraKp {
  String id_prakp;
  String nim;
  String tahun;
  String semester;
  String judul;
  String pembimbing;
  String status;



  PraKp(
      {this.id_prakp, this.nim, this.tahun, this.semester, this.judul, this.pembimbing, this.status});

  factory PraKp.fromJson(Map<String, dynamic> map){
    return PraKp(
        id_prakp: map["id_prakp"],
        nim: map["nim"],
        tahun: map["tahun"],
        semester: map["semester"],
        judul: map["judul"],
        pembimbing: map["pembimbing"],
        status: map["status"]);

  }

  Map<String, dynamic> toJson() {
    return {
      "id_prakp": id_prakp,
      "nim": nim,
      "tahun": tahun,
      "semester": semester,
      "judul": judul,
      "pembimbing": pembimbing,
      "status": status,
    };
  }

  @override
  String toString() {
    return 'PraKp{id_prakp: $id_prakp, nim: $nim, tahun: $tahun, semester: $semester, judul: $judul, pembimbing: $pembimbing, status: $status}';
  }
}

List<PraKp> prakpFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<PraKp>.from(data.map((item) => PraKp.fromJson(item)));
}

String prakpToJson(KP data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

//------------------------------------------------------------------------------------------------------------------------------------------

class Sk {
  String id_skp;
  String tahun;
  String semester;
  String nim;
  String isi_surat;
  String status;



  Sk(
      {this.id_skp, this.tahun, this.semester,  this.nim, this.isi_surat,this.status});

  factory Sk.fromJson(Map<String, dynamic> map){
    return Sk(
        id_skp: map["id_skp"],
        tahun: map["tahun"],
        semester: map["semester"],
        nim: map["nim"],
        isi_surat: map["isi_surat"],
        status: map["status"]);

  }

  Map<String, dynamic> toJson() {
    return {
      "id_skp": id_skp,
      "tahun": tahun,
      "semester": semester,
      "nim": nim,
      "isi_surat": isi_surat,
      "status": status,
    };
  }

  @override
  String toString() {
    return 'Sk{id_skp: $id_skp, tahun: $tahun, semester: $semester, nim: $nim, isi_surat: $isi_surat, status: $status}';
  }
}

List<Sk> skFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Sk>.from(data.map((item) => Sk.fromJson(item)));
}

String skToJson(KP data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Kpbatas {
  String id_batas_kp;
  String id_kp;
  String tanggal_batas;


  Kpbatas(
      {this.id_batas_kp, this.id_kp, this.tanggal_batas});

  factory Kpbatas.fromJson(Map<String, dynamic> map){
    return Kpbatas(
        id_batas_kp: map["id_batas_kp"],
        id_kp: map["id_kp"],
        tanggal_batas: map["tanggal_batas"]);

  }

  Map<String, dynamic> toJson() {
    return {
      "id_batas_kp": id_kp,
      "id_kp": id_kp,
      "tanggal_batas": tanggal_batas,

    };
  }

  @override
  String toString() {
    return 'Kpbatas{id_batas_kp: $id_batas_kp, id_kp: $id_kp, tanggal_batas: $tanggal_batas}';
  }
}

List<Kpbatas> bataskpFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Kpbatas>.from(data.map((item) => Kpbatas.fromJson(item)));
}

String bataskpToJson(Kpbatas data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}


